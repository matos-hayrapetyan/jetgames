<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * @property mixed src
 */
class Media extends Model
{

    protected $fillable = ['src'];

    public function getFile()
    {
        return Storage::disk('local')->url($this->src);
    }

    public function games()
    {
        return $this->hasMany('App\Game');
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }
}
