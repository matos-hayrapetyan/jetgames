<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['user_id', 'plan_id', 'status', 'valid_from'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function isActive()
    {
        return (!$this->isCanceled() && ($this->hasPayments() && $this->isPaymentOverdue()));
    }

    public function isCanceled()
    {
        return ('canceled' === $this->status);
    }

    public function isPaymentOverdue()
    {
        return $this->daysSinceLastPayment() > $this->plan()->billing_frequency;
    }

    public function daysSinceLastPayment()
    {
        return Carbon::now()->diffInDays(Carbon::createFromFormat('Y-m-d H:i:s', $this->getLastPayment()->date));
    }

    public function hasPayments()
    {
        return !$this->payments()->get()->isEmpty();
    }

    public function getLastPayment()
    {
        return $this->payments()->orderBy('date', 'DESC')->first();
    }
}
