<?php

/**
 * @param $url string
 * @return string
 */
function nonSSL($url){
    return str_replace('https:','http:',$url);
}