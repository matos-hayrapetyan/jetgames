<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{


    protected $fillable = ['name','slug','content','description','image_id','preview_image_id', 'featured_tag_id','status','requires_premium'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }


    /**
     * Get the image record associated with the game.
     */
    public function image()
    {
        return $this->belongsTo('App\Media');
    }

    /**
     * Get the image record associated with the game.
     */
    public function preview_image()
    {
        return $this->belongsTo('App\Media');
    }

    public function featured_tag()
    {
        return $this->belongsTo('App\Tag','featured_tag_id','id');
    }

}
