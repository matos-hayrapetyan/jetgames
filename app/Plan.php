<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['package_id','name','slug','description','price','trial_days','billing_frequency','moneyback','ordering','public'];

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
