<?php

namespace App\Listeners;

use App\Events\WikiUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use IXR\Client\Client;

class SendXmlRpcWikiRequests implements ShouldQueue
{
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'database';

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'default';

    public $servers = [
        'http://rpc.pingomatic.com/',
        'http://rpc.pingomatic.com',
        'http://rpc.twingly.com',
        'http://api.feedster.com/ping',
        'http://api.moreover.com/RPC2',
        'http://api.moreover.com/ping',
        'http://www.blogdigger.com/RPC2',
        'http://www.blogshares.com/rpc.php',
        'http://www.blogsnow.com/ping',
        'http://www.blogstreet.com/xrbin/xmlrpc.cgi',
        'http://bulkfeeds.net/rpc',
        'http://www.newsisfree.com/xmlrpctest.php',
        'http://ping.blo.gs/',
        'http://ping.feedburner.com',
        'http://ping.syndic8.com/xmlrpc.php',
        'http://ping.weblogalot.com/rpc.php',
        'http://rpc.blogrolling.com/pinger/',
        'http://rpc.technorati.com/rpc/ping',
        'http://rpc.weblogs.com/RPC2',
        'http://www.feedsubmitter.com',
        'http://blo.gs/ping.php',
        'http://www.pingerati.net',
        'http://www.pingmyblog.com',
        'http://geourl.org/ping',
        'http://ipings.com',
        'http://www.weblogalot.com/ping',
    ];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WikiUpdated  $event
     * @return void
     */
    public function handle(WikiUpdated $event)
    {
        $home = url('/');
        $rss_url = url('/feeds/new-wiki-articles');
        foreach($this->servers as $server){
            $client = new Client($server,false,80,3);
            if ( !$client->query('weblogUpdates.extendedPing', 'Jetgames', $home, $rss_url ) ) // then try a normal ping
                $client->query('weblogUpdates.ping', 'Jetgames', $home);
        }
        Log::info('XML-RPC done');
    }
}
