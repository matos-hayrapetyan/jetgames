<?php

namespace App\Http\Controllers\Wiki;

use App\Article;
use App\Events\WikiUpdated;
use App\Http\Controllers\Controller;
use App\Media;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{

    /**
     * GET /wiki
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::orderBy('created_at','desc')->paginate(40);

        if (request()->ajax()) {
            return view('wiki.articles.index', compact('articles'));
        }

        return view('wiki.index',compact('articles'));
    }

    /**
     * GET /wiki/{article}
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Article $article)
    {
        return view('wiki.articles.show', compact('article'));
    }

    /**
     * GET /wiki/create
     */
    public function create()
    {
        return view ('wiki.articles.create');
    }

    /**
     * POST /wiki
     */
    public function store()
    {


        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;

        $src = request()->file('image')->store($path);

        $image = Media::create(['src'=>$src]);


        $this->validate(request(),[
            'name' => 'required',
            'slug' => 'required',
            'content' => 'required',
        ]);

        $newArticle = Article::create([
            'name' => request('name'),
            'slug' => request('slug'),
            'content' => request('content'),
            'meta_description' => request('meta_description'),
            'meta_keywords' => request('meta_keywords'),
            'image_id' => $image->id,
        ]);

        session()->flash('message','Article created success!');

        event(new WikiUpdated());

        return redirect('wiki/'.$newArticle->slug.'/edit');
    }

    public function edit(Article $article)
    {
        return view('wiki.articles.edit', compact('article'));
    }

    /**
     * PUT/PATCH /wiki/{article}
     *
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Article $article)
    {
        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;

        $this->validate(request(),[
            'name' => 'required',
            'slug' => 'required',
            'content' => 'required',
        ]);

        $article->name = request('name');
        $article->slug = request('slug');
        $article->content = request('content');
        $article->meta_description = request('meta_description');
        $article->meta_keywords = request('meta_keywords');

        if(!empty(request()->file('image'))){
            $src = request()->file('image')->store($path);
            $image = Media::create(['src'=>$src]);
            $article->image_id = $image->id;
        }

        $article->save();

        session()->flash('message','Article updated success!');

        event(new WikiUpdated());

        return redirect('wiki/'.$article->slug.'/edit');
    }

}
