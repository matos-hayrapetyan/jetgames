<?php

namespace App\Http\Controllers;

use App\Game;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $games = Game::orderBy('created_at','desc')->where('status','published')->paginate(40);

        if (request()->ajax()) {
            return view('games.index', compact('games'));
        }

        if(!Auth::check()){
            $landingTags = $this->getLandingTags();
        }else{
            $landingTags = $this->getUserLandingTags();
        }

        return view('home.index',compact('games','landingTags'));
    }

    public function getLandingTags()
    {
        $results= [];

        $results['Race for Glory'] = Tag::where('slug','racing')->with(['games' => function($query){
            $query->limit(10);
        }])->first();
        $results['Rule the Arcade'] = Tag::where('slug','arcade')->with(['games'=>function($query){
            $query->limit(10);
        }])->first();

        return $results;
    }

    public function getUserLandingTags()
    {
        return $this->getLandingTags();
    }
}
