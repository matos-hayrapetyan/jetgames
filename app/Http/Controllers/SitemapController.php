<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Game;
use App\Tag;

class SitemapController extends Controller
{
    public function index()
    {
        $game = Game::orderBy('updated_at', 'desc')->first();
        $tag = Tag::orderBy('updated_at', 'desc')->first();
        $article = Article::orderBy('updated_at', 'desc')->first();

        return response()->view('sitemap.index', [
            'game' => $game,
            'tag' => $tag,
            'article' => $article,
        ])->header('Content-Type', 'text/xml');
    }

    public function games()
    {
        $games = Game::orderBy('updated_at', 'desc')->where('status','published')->get();
        return response()->view('sitemap.games', [
            'games' => $games,
        ])->header('Content-Type', 'text/xml');
    }

    public function tags()
    {
        $tags = Tag::orderBy('updated_at', 'desc')->where('status','published')->get();
        return response()->view('sitemap.tags', [
            'tags' => $tags,
        ])->header('Content-Type', 'text/xml');
    }

    public function wiki()
    {
        $articles = Article::orderBy('updated_at', 'desc')->get();

        return response()->view('sitemap.wiki', [
            'articles' => $articles,
        ])->header('Content-Type', 'text/xml');

    }
}
