<?php

namespace App\Http\Controllers;

use App\Events\GameUpdated;
use Illuminate\Http\Request;
use App\Game;
use App\Tag;
use App\Media;
use Carbon\Carbon;

class GamesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * GET / & /games index
     */
    public function index()
    {
        $games = Game::orderBy('updated_at','desc')->where('status','published')->paginate(40);

        if (request()->ajax()) {
            return view('games.index', compact('games'));
        }

        return view('games.landing',compact('games'));
    }

    /**
     * GET /games/{slug}
     *
     * @param Game $game
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Game $game)
    {
        $tags = $game->tags->modelKeys();
        $similar_games = Game::whereHas('tags', function ($q) use ($tags) {
            $q->whereIn('tags.id', $tags);
        })->where('id', '<>', $game->id)->inRandomOrder()->take(10)->get();


        return view('games.show', compact('game', 'similar_games'));
    }

    /**
     * GET /games/create create
     */
    public function create()
    {
        $tags = Tag::all();

        return view('games.create', compact('tags'));
    }

    /**
     * POST /games
     */
    public function store(Request $request)
    {
        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;


        $this->validate(request(),[
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'content' => 'required',
            'tag_id' => 'required|array|min:1',
        ]);

        $newGame = Game::create([
            'name' => request('name'),
            'slug' => request('slug'),
            'description' => request('description'),
            'content' => request('content'),
            'featured_tag_id' => request('featured_tag'),
            'status'=>request('status'),
            'requires_premium' => $request->has('requires_premium')
        ]);

        if(!empty(request()->file('image'))){
            $src = request()->file('image')->store($path);
            $image = Media::create(['src'=>$src]);
            $newGame->image_id = $image->id;
        }

        if(!empty(request()->file('image'))){
            $preview_src = request()->file('preview_image')->store($path);

            $preview_image = Media::create(['src'=>$preview_src]);
            $newGame->preview_image_id = $preview_image->id;
        }

        $newGame->save();


        $newGame->tags()->attach(request('tag_id'));

        session()->flash('message','Game created success!');

        event(new GameUpdated($newGame));

        return redirect('games/'.$newGame->slug.'/edit');

    }

    /**
     * GET /games/{id}/edit
     */
    public function edit(Game $game)
    {
        $tags= $game->tags->modelKeys();


        return view('games.edit', compact('game','tags'));
    }

    /**
     * PUT/PATCH /games/{id}
     */
    public function update(Request $request)
    {
        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;


        $this->validate(request(),[
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'content' => 'required',
            'tag_id' => 'required|array|min:1',
        ]);

        $game = Game::where('id', request('game_id'))->first();
        $game->name = request('name');
        $game->slug = request('slug');
        $game->description = request('description');
        $game->content = request('content');
        $game->featured_tag_id = request('featured_tag');
        $game->requires_premium = $request->has('requires_premium');
        $game->status = request('status');

        if(!empty(request()->file('image'))){
            $src = request()->file('image')->store($path);
            $image = Media::create(['src'=>$src]);
            $game->image_id = $image->id;
        }

        if(!empty(request()->file('preview_image'))){
            $previewSrc = request()->file('preview_image')->store($path);
            $previewImage = Media::create(['src'=>$previewSrc]);
            $game->preview_image_id = $previewImage->id;
        }

        $game->save();

        $game->tags()->detach();
        $game->tags()->attach(request('tag_id'));

        session()->flash('message','Game updated success!');

        event(new GameUpdated($game));

        return redirect('games/'.$game->slug.'/edit');

    }

    /**
     * DELETE /games/{id}
     */
    public function destroy()
    {

    }

}
