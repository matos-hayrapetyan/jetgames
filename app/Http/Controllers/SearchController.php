<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Tag;

class SearchController extends Controller
{
    public function index($keyword)
    {

        $games = Game::where("name", "LIKE","%$keyword%")->orderBy('updated_at','desc')->where('status','published')->paginate(40);
        if (request()->ajax()) {
            return view('games.index', compact('games'));
        }

        return view('search.index', compact('games','keyword'));
    }
}
