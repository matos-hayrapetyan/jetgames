<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::withCount('games')->orderBy('games_count','desc')->get();

        return view('admin.tags.index',compact('tags'));
    }
}
