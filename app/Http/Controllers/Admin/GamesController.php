<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GamesController extends Controller
{
    public function index()
    {
        $games = Game::orderBy('updated_at','desc')->get();
        return view('admin.games.index',compact('games'));
    }
}
