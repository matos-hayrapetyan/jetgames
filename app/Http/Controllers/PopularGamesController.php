<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Tag;


class PopularGamesController extends Controller
{
    public function index(){
        $games = Game::orderBy('created_at','asc')->where('status','published')->paginate(40);

        if (request()->ajax()) {
            return view('games.index', compact('games'));
        }

        return view('popular-games.index', compact('games'));
    }
}
