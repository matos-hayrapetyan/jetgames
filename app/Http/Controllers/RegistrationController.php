<?php

namespace App\Http\Controllers;

use App\Mail\UserVerify;
use App\Role;
use App\Rules\Recaptcha;
use App\User;
use Illuminate\Support\Facades\Mail;

class RegistrationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * GET /register
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view('register.create');
    }

    public function store()
    {

        $this->validate(request(), [
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'g-recaptcha-response' => ['required',new Recaptcha]
        ]);

        $confirmation_code = str_random(30);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'confirmation_code' => $confirmation_code
        ]);

        $user
            ->roles()
            ->attach(Role::where('name', 'member')->first());


        Mail::to($user)->send(new UserVerify($confirmation_code));

        session()->flash('message','Thanks for signing up! Please check your email.');

        auth()->login($user,true);

        return redirect('/');
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new \Exception('Invalid confirmation code');
        }

        $user = User::where('confirmation_code',$confirmation_code)->first();

        if ( ! $user)
        {
            throw new \Exception('Invalid confirmation code');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        session()->flash('message','Account activated successfully!');

        auth()->login($user);

        return redirect('/control-panel');
    }

    public function resend($email)
    {
        $user = User::where('email',$email)->first();

        if(!$user){
            throw new \Exception('User with specified email does not exist');
        }

        if(empty($user->confirmation_code)){
            $user->confirmation_code = str_random(30);
        }

        Mail::to($user)->send(new UserVerify($user->confirmation_code));

        session()->flash('message','Please check your email for verification mail.');

        return redirect('/');
    }
}
