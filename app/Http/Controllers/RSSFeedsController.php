<?php

namespace App\Http\Controllers;

use App\Article;
use App\Game;
use Illuminate\Http\Request;

class RSSFeedsController extends Controller
{
    public function games()
    {
        $games = Game::orderBy('updated_at', 'desc')->where('status','published')->limit(100)->get();
        return response()->view('rss-feed.new-games', [
            'games' => $games,
        ])->header('Content-Type', 'application/rss+xml');
    }

    public function wiki()
    {
        $articles = Article::orderBy('updated_at', 'desc')->limit(50)->get();
        return response()->view('rss-feed.new-wiki-articles', [
            'articles' => $articles,
        ])->header('Content-Type', 'application/rss+xml');
    }
}
