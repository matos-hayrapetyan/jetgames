<?php

namespace App\Http\Controllers;

use App\Rules\Recaptcha;
use App\Rules\UserVerified;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Tag;

class SessionsController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Validate the user login request.
     *
     * Todo: do we require user to verify email for logging in?
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => ['required','string','exists:users,email'/*,new UserVerified*/],
            'password' => 'required|string',
            'g-recaptcha-response' => ['required',new Recaptcha]
        ]);


    }

    public function index()
    {
        return view('sessions.index');
    }

    public function create()
    {

        return view('sessions.create');
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
