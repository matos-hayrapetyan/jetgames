<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PremiumSubscriptionsController extends Controller
{

    public function index()
    {
        \ChargeBee_Environment::configure("jetgames-test", "test_JfdhRpQgoTxnH6DmcgaQ0o6I4Sy97ghE");
        $result = \ChargeBee_HostedPage::checkoutNew(array(
            "subscription" => array(
                "planId" => "jetgames-premium-plan"
            )));
        $hostedPage = $result->hostedPage();

        return view('subscribe.premium.index', compact('hostedPage'));
    }

}
