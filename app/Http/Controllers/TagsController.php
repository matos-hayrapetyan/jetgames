<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Media;
use App\Tag;

class TagsController extends Controller
{

    public function index()
    {
        $tags = Tag::withCount('games')->orderBy('games_count','desc')->get();
        if(request()->ajax()){
            return response()->json($tags);
        }


        return view('tags.index', compact('tags'));

    }

    public function show(Tag $tag)
    {
        $games = $tag->games()->orderBy('updated_at','desc')->where('status','=','published')->paginate(40);

        if (request()->ajax()) {
            return view('games.index', compact('games'));
        }

        return view('tags.show', compact('tag', 'games'));
    }

    public function create()
    {

        return view('tags.create');
    }

    public function store()
    {
        $date = Carbon::now();
        $path = 'public/images/' . $date->year . '/' . $date->month;

        $src = request()->file('image')->store($path);

        $image = Media::create(['src' => $src]);

        $this->validate(request(), [
            'name' => 'required',
            'slug' => 'required',
        ]);

        Tag::create([
            'name' => request('name'),
            'slug' => request('slug'),
            'image_id' => $image->id,
            'description' => (!empty(request('description')) ? request('description') : ''),
        ]);

        session()->flash('message','Tag created success!');

        return redirect('tags/create');
    }

    /**
     * GET /tags/{id}/edit
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact('tag'));
    }

    /**
     * PUT/PATCH /tags/{id}
     */
    public function update()
    {
        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;

        $this->validate(request(),[
            'name' => 'required',
            'slug' => 'required',
        ]);

        $tag = Tag::where('id', request('tag_id'))->first();
        $tag->name = request('name');
        $tag->slug = request('slug');
        $tag->description = (!empty(request('description')) ? request('description') : '');

        if(!empty(request()->file('image'))){
            $src = request()->file('image')->store($path);
            $image = Media::create(['src'=>$src]);
            $tag->image_id = $image->id;
        }

        $tag->save();

        session()->flash('message','Tag updated success!');

        return redirect('tags/'.$tag->slug.'/edit');

    }
}
