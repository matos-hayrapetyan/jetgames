<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;

class MediaController extends Controller
{
    public function store()
    {
        $date = Carbon::now();
        $path = 'public/images/'.$date->year.'/'.$date->month;

        $src = request()->file('file')->store($path);

        return url('/').Storage::disk('local')->url($src);
    }
}
