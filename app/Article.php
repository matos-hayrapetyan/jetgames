<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = ['name','slug','content','meta_description','meta_keywords','image_id'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function getMetaDescriptionAttribute($value){
        if(!empty($value)){
            return $value;
        }

        if(!empty($this->content)){
            return mb_substr(strip_tags ($this->content), 0, 157) .'...';
        }

        return $value;
    }

    public function getMetaKeywordsAttribute($value)
    {
        if(!empty($value)){
            return $value;
        }

        if(!empty($this->name)){
            return $this->name . ', Play Online Games, Free Online Games';
        }

        return $value;
    }

    /**
     * Get the image record associated with the game.
     */
    public function image()
    {
        return $this->belongsTo('App\Media');
    }

}
