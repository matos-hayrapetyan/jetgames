<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['name','slug','description','public'];

    public function plans()
    {
        return $this->hasMany('App\Plan');
    }
}
