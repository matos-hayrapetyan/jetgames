<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['subscription_id','price','date','status'];


    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
}
