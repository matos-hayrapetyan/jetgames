<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('control-panel', 'SessionsController@index')->middleware('auth');

Route::get('login', 'SessionsController@create')->middleware('guest')->name('login');
Route::post('login', 'SessionsController@login')->middleware('guest');

Route::get('logout', 'SessionsController@destroy')->middleware('auth');

Route::get('register', 'RegistrationController@create')->middleware('guest');
Route::post('register', 'RegistrationController@store')->middleware('guest');

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegistrationController@confirm'
]);

Route::get('register/verify/resend/{email}', [
    'as' => 'email',
    'uses' => 'RegistrationController@resend'
]);


Route::resource('games', 'GamesController');
Route::resource('tags', 'TagsController');

Route::post('media','MediaController@store');

Route::get('popular-games', 'PopularGamesController@index');


Route::get('search/{keyowrd}', 'SearchController@index');

// Sitemap
Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap-pt-games.xml', 'SitemapController@games');
Route::get('/sitemap-pt-tags.xml', 'SitemapController@tags');
Route::get('/sitemap-pt-wiki.xml', 'SitemapController@wiki');

// RSS Feed
Route::get('/feeds/new-games', 'RSSFeedsController@games');
Route::get('/feeds/new-wiki-articles', 'RSSFeedsController@wiki');

Route::namespace('Wiki')->prefix('wiki')->group(function(){
    Route::get('/', 'ArticlesController@index');

    Route::get('/create', 'ArticlesController@create')->middleware('auth','auth.admin');
    Route::post('/', 'ArticlesController@store')->middleware('auth','auth.admin');

    Route::get('/{article}', 'ArticlesController@show');

    Route::put('/{article}', 'ArticlesController@update')->middleware('auth','auth.admin');
    Route::patch('/{article}', 'ArticlesController@update')->middleware('auth','auth.admin');
    Route::get('/{article}/edit', 'ArticlesController@edit')->middleware('auth','auth.admin');
});

Route::namespace('Admin')->prefix('admin')->middleware('auth','auth.admin')->group(function(){
    Route::get('/games', 'GamesController@index');
    Route::get('/tags', 'TagsController@index');
});

Route::get('/subscribe/premium', 'PremiumSubscriptionsController@index');
