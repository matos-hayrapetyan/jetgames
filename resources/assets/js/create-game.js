var tagSelect = document.getElementById('edit_tags'),
    featuredTagSelect = document.getElementById('edit_featured_tag');

new Vue({
    el: '#app',
    data: {
        csrf_token: document.querySelector('input[name="_token"]').getAttribute('value'),
        mediaSrc: '',
        fileUploadData: '',
        tags: [],
        selectedTags: null !== tagSelect ? JSON.parse(tagSelect.getAttribute('data-default')): [],
        selectedFeaturedTag: null !== featuredTagSelect ? featuredTagSelect.getAttribute('data-default'): '',
    },
    methods: {
        uploadFile: function (e) {
            e.preventDefault();
            var file = document.querySelector('#mediaUpload').files[0];

            var formData = new FormData();
            var _this = this;

            formData.append('_token',this.csrf_token);
            formData.append('file', file);

            this.$http.post('/media', formData).then(function(data){
                console.log(data.body);
                _this.mediaSrc = data.body;
            });
        },
        loadData: function(){
            this.$http.get('/tags').then(function (response) {
                this.tags = response.body;
            }.bind(this));
        },
        inArray: function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        }

    },
    mounted: function () {

        this.loadData();

        setInterval(function () {
            this.loadData();
            console.log(this.selectedTags);
        }.bind(this), 5000);
    }
});