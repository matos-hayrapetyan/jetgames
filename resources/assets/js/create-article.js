new Vue({
    el: '#app',
    data: {
        csrf_token: document.querySelector('input[name="_token"]').getAttribute('value'),
        mediaSrc: '',
        fileUploadData: '',
        tags: '',
    },
    methods: {
        uploadFile: function (e) {
            e.preventDefault();
            var file = document.querySelector('#mediaUpload').files[0];

            var formData = new FormData();
            var _this = this;

            formData.append('_token',this.csrf_token);
            formData.append('file', file);

            this.$http.post('/media', formData).then(function(data){
                _this.mediaSrc = data.body;
            });
        }

    }
});