window.Vue = require('vue');
Vue.use(require('vue-resource'));


jQuery(document).ready(function(){
    /** Game Previews */
    jQuery('.nav-burger').on('click tap',function(){
        jQuery(this).closest('nav').find('.nav-items').toggleClass('burger-open');
        return false;
    });

    /*jQuery('body').on('mouseenter','.card--game .card-img',function(){
        jQuery(this).find('img').attr('src',jQuery(this).data('preview'));
    });

    jQuery('body').on('mouseleave','.card--game .card-img',function(){
        jQuery(this).find('img').attr('src',jQuery(this).data('img'));
    });*/

    if(jQuery('.games-index-list').length){
        doInfiniteScroll();
    }

    if(jQuery('.articles-index-list').length){
        doArticlesInfiniteScroll();
    }

    function doArticlesInfiniteScroll(){
        var flag = false,
            list = jQuery('.articles-index-list'),
            total = + list.data('total');
        jQuery(window).scroll(function(){
            if(flag)
                return; //return if the infinite pagination is calling
            if  ((jQuery(window).scrollTop() >= jQuery('.articles-index-list > div.card').last().offset().top - 400) || (jQuery(window).scrollTop() >= (jQuery(document).height() - jQuery(window).height())*90/100)){
                flag = true;  //set flag
                infiniteLoad();
            }
        });

        function infiniteLoad(){
            var currentPage = list.data('paged'),
                nextPage = + currentPage+1;
            if(currentPage === total){
                return;
            }
            jQuery.ajax({
                url: 'wiki/?page='+nextPage,
            }).done(function(data){
                flag = false; //un set flag

                list.append(data).data('paged',nextPage);
            })
            //do something
        }
    }


    function doInfiniteScroll(){
        var flag = false,
            list = jQuery('.games-index-list'),
            total = + list.data('total');
        jQuery(window).scroll(function(){
            if(flag)
                return; //return if the infinite pagination is calling
            if  ((jQuery(window).scrollTop() >= jQuery('.games-index-list > div.card').last().offset().top - 400) || (jQuery(window).scrollTop() >= (jQuery(document).height() - jQuery(window).height())*90/100)){
                flag = true;  //set flag
                infiniteLoad();
            }
        });

        function infiniteLoad(){
            var currentPage = list.data('paged'),
                nextPage = + currentPage+1;
            if(currentPage === total){
                return;
            }
            jQuery.ajax({
                url: '?page='+nextPage,
            }).done(function(data){
                flag = false; //un set flag

                list.append(data).data('paged',nextPage);
            })
            //do something
        }
    }
});