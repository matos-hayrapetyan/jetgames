@extends('layouts.app')

@section('title', 'All free games categories. Online game tags, Play Free Online Games')

@section('meta')
    <meta name="description" content="All online game categories, choose your game tag and play most popular online games" />
    <meta name="keywords" content="online games, game tags, pc games" />
@endsection

@section('content')

    <div class="flex flex-grid justify-center container margin-40-v">
        @foreach($tags as $tag)
            <div class="tag-index card no-padding margin-10">
                <a href="/tags/{{ $tag->slug }}">
                    <img src="{{ $tag->image->getFile() }}" alt="{{ $tag->name }}"/>
                    <span class="tag-name">{{ $tag->name }}</span>
                    <span class="tag-count">{{ $tag->games_count }}</span>
                </a>
            </div>
        @endforeach
    </div>

@endsection