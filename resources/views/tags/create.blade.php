@extends('layouts.admin')

@section('title', 'Publish a new tag')

@section('content')

    <div class="container margin-40-v card">
        <form action="{{ url('tags') }}" method="post" enctype="multipart/form-data">
            <h1>Publish a new tag</h1>
            {{ csrf_field() }}
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" />
            </div>
            <div class="form-field">
                <label for="image">Image</label>
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <label for="description">Description</label>
                <textarea name="description" id="description"></textarea>
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection