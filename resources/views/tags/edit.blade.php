@extends('layouts.admin')

@section('title', 'Editing a tag')

@section('content')

    <div class="container margin-40-v card">
        <form action="{{ url('tags/'.$tag->slug) }}" method="post" enctype="multipart/form-data">
            <h1>Editing a tag</h1>
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <input type="hidden" name="tag_id" id="tag_id" value="{{ $tag->id }}" />
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="{{ $tag->name }}" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" value="{{ $tag->slug }}" />
            </div>
            <div class="form-field">
                <label for="description">Description</label>
                <textarea name="description" id="description">{{ $tag->description }}</textarea>
            </div>
            <div class="form-field">
                <label for="image">Image</label>
                @if($tag->image)
                    <div>
                        <img src="{{ $tag->image->getFile() }}" />
                    </div>
                @endif
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection