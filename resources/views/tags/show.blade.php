@extends('layouts.app')

@section('title', $tag->name.' Games, Free Online '.$tag->name.' Games, Play '.$tag->name.' Games Online')


@section('meta')
    <meta name="description" content="{{ $tag->name }} Games Play Free Online. Play Now {{ $tag->name }} games at JetGames" />
    <meta name="keywords" content="{{ $tag->name }} games, {{ $tag->name }} games, play online {{ $tag->name }}  games, pc games" />
@endsection

@section('content')

    <div class="container card margin-40-top text-center">
        <h1 class="tag-title"><img style="vertical-align: middle" src="{{ $tag->image->getFile() }}" alt="{{ $tag->name }}" />&nbsp;&nbsp;{{ $tag->name }} Games, Free Online {{ $tag->name }} Games, Play {{ $tag->name }} Games Online</h1>
        @if(Auth::check() && auth()->user()->hasRole('admin'))
            <h2 class="text-left"><a href="{{ url('tags/'.$tag->slug.'/edit') }}">Edit Tag</a></h2>
        @endif
    </div>

    <div class="games-index-list container flex flex-grid margin-40-v" data-paged="{{ $games->currentPage() }}" data-total="{{ $games->lastPage() }}">

        @include('games.index')

    </div>

    @if(!empty($tag->description))
    <div class="margin-40-v card container text-justify">
        <div class="text-content">
            {!! $tag->description !!}
        </div>
    </div>
    @endif
@endsection