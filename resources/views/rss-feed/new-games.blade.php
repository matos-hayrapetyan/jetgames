<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
     xmlns:media="http://search.yahoo.com/mrss/"
>

    <channel>
        <title>Jetgames</title>
        <atom:link href="{{ Request::url() }}" rel="self" type="application/rss+xml" />
        <link>{{ url('/') }}</link>
        <description>The most creative and exciting online games for all renaissance and sexes on a unique gaming portal jetgames.online. Here are some fascinating and exclusive novelties of the game world</description>
        <lastBuildDate>{{ $games[0]->updated_at->format("D, d M Y H:i:s T") }}</lastBuildDate>
        <language>en-US</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        @foreach($games as $game)
        <item>
            <title>{{ $game->name }}</title>
            <link>{{ nonSSL(url('/games/'.$game->slug)) }}</link>
            <pubDate>{{ $game->created_at->format("D, d M Y H:i:s T") }}</pubDate>
            <dc:creator/>
            <guid isPermaLink="false">{{ $game->id }}</guid>
            <description><![CDATA[{{ $game->description }}]]></description>
            <dc:creator/>
            <media:content/>
            @foreach($game->tags as $tag)
                <category>{{ $tag->name }} Games</category>
            @endforeach
        </item>
        @endforeach
    </channel>
</rss>
