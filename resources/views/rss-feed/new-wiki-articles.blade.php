<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
     xmlns:media="http://search.yahoo.com/mrss/"
>

    <channel>
        <title>Jetgames Wiki</title>
        <atom:link href="{{ Request::url() }}" rel="self" type="application/rss+xml" />
        <link>{{ url('/') }}</link>
        <description>Online Games Encyclopedia</description>
        <lastBuildDate>{{ $articles[0]->updated_at->format("D, d M Y H:i:s T") }}</lastBuildDate>
        <language>en-US</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        @foreach($articles as $article)
            <item>
                <title>{{ $article->name }}</title>
                <link>{{ url('/wiki/'.$article->slug) }}</link>
                <pubDate>{{ $article->created_at->format("D, d M Y H:i:s T") }}</pubDate>
                <dc:creator/>
                <guid isPermaLink="false">{{ $article->id }}</guid>
                <description/>
                <dc:creator/>
                <media:content/>
            </item>
        @endforeach
    </channel>
</rss>
