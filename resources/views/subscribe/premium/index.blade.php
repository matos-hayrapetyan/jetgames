@extends('layouts.app')

@section('title', 'Unlimited Fun. Cool Gifts. At the price of coffee.')

@section('content')
    <div id="premium_landing" class="landing">
        <div class="row hero">
            <div class="col">
                <h1 class="hero-title">Unlimited Fun. Cool Gifts.<br /> At the price of coffee.</h1>
                <p class="hero-subtitle">Move your business forward with Google Cloud Platform</p>
                <div>
                    <a href="{{ $hostedPage->url }}" class="button button--big" target="_blank">GET NOW</a>
                </div>
            </div>
            <div class="col">
                <div class="image-wrap">
                    <img src="{{ url('images/jetgames.png') }}" alt="jetgames" />
                </div>
            </div>
        </div>
        <div id="premium_benefits" class="text-center">
            <h2 class="premium-benefits-title">Premium Benefits</h2>
            <p class="premium-benefits-lead">GCP frees you from the overhead of managing infrastructure, provisioning servers and configuring networks. To let innovators innovate and let coders, well, just code.</p>
            <div class="flex">
                <a href="{{ url('games/premium') }}">

                </a>
            </div>
        </div>
    </div>

@endsection