@extends('layouts.app')

@section('title', 'Popular Games, Play Online Popular Games, Free Online Popular Games, Most Popular Games Online')

@section('meta')
    <meta name="description" content="Worlds popular online games collected in one resource. Many arduous years of subtle observation and trials led to the formation of this universal assembly of  this unique catalog. Popular games are collected even for the most demanding" />
    <meta name="keywords" content="popular games, online games, popular online games, free popular games" />
@endsection

@section('content')

    <div class="margin-40-top card container text-center">
        <h1>Popular Games, Play Online Popular Games, Free Online Popular Games, Most Popular Games Online</h1>
    </div>

    <div class="games-index-list container flex flex-grid margin-40-v" data-paged="{{ $games->currentPage() }}" data-total="{{ $games->lastPage() }}">
        @include('games.index')
    </div>

@endsection