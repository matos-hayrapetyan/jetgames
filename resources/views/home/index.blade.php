@extends('layouts.app')

@section('title', 'Free Games, Kids Games, Play Action Games, Arcade, PC Games')


@section('meta')
    <meta name="description"
          content="The most creative and exciting online games for all renaissance and sexes on a unique gaming portal jetgames.online. Here are some fascinating and exclusive novelties of the game world"/>
    <meta name="keywords" content="free games, kids games, online games"/>
@endsection


@section('body-classes', 'home-body-landing')


@section('before-header')
    <div class="home-landing background-landing"></div>
@endsection


@section('content')
    <div id="intro">
        <div class="home-container">
            <div class="intro-headline">All favorite games in one place</div>
            <a class="intro-link" href="{{ url('/games') }}">View All Games</a>
            {{--<a class="intro-link premium" href="{{  url('/subscribe/premium')  }}">Get Premium for $1.99</a>--}}
        </div>

    </div>

    {{--<div class="container">
        @foreach($landingTags as $title=>$landingTag)
            <div class="cluster">
                <div class="cluster-heading">
                    <h2 class="cluster-title"><a href="{{ url('/tags/'.$landingTag->slug) }}">{{ $title }}</a></h2>
                    <a class="see-more button" href="{{ url('/tags/'.$landingTag->slug) }}">See more</a>
                </div>
                <div class="card-list">
                    @foreach($landingTag->games as $game)
                        <div class="card card--game card-small card--overlay no-padding">
                            <a href="/games/{{ $game->slug }}">
                                <div class="card-img">
                                    <img src="{{ $game->image->getFile() }}" alt="{{ $game->name }}"/>
                                </div>
                                <div class="card-meta">
                                    <h2 class="card-head">{{ $game->name }}</h2>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>--}}

    <div class="jetgames container text-center">
        <h1 class="-primary">Play Online Games, Free Online <a href="{{ url('/tags/kids') }}">Games for Kids</a> , Play <a
                    href="{{ url('tags/action') }}">Action</a> Games, <a href="{{ url('tags/arcade') }}">Arcade</a>, PC
            Games</h1>
    </div>

    <div class="games-index-list container flex flex-grid margin-40-v" data-paged="{{ $games->currentPage() }}"
         data-total="{{ $games->lastPage() }}">
        @include('games.index')
    </div>

    <div class="margin-40-v card container text-justify">
        <div class="text-content">
            Welcome to the hottest and most creative game portal. If you are looking for the most popular and new online
            games, then you have come to the right address.
            Our team represents all possible genres of games that will make you feel fun and moving, all sorts of
            puzzles and games for the girl, games for boys and exclusive logic games.
            Our portal is created to make offer even for the most demanding gamers. Will be presented as the exciting
            games themselves, and reviews and comparison of these games, will show a variety of keys for them and
            strategies. A huge assortment of multiplayer games will be presented.
            Visitors are waiting for pleasant surprises and surprisingly the newest novelties in the game world.
            We get involved in the multi-year experience and the results of research in this field and know what our
            visitors are waiting for from us.
            We look forward to seeing you on our online games portal, users of all ages and genders, you will find a
            game of your dreams.
        </div>
    </div>

@endsection