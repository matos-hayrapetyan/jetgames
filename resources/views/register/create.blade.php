@extends('layouts.app')

@section('header-scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('title', 'Register')

@section('content')
    <script>
        function JSF(){
            document.getElementById("reg-form").submit();
        }
    </script>
    <div id="register-wrap" class="margin-40-v card">
        <form action="{{ url('/register') }}" method="post" id="reg-form">
            {{ csrf_field() }}
            <div class="form-field">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" />
            </div>
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
            </div>
            <div class="form-field">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" />
            </div>
            <div class="form-field">
                <button
                        type="submit"
                        class="g-recaptcha button"
                        data-sitekey="6LdPWzQUAAAAABIbD3s1GHFrzy-tjhkPFuImsN1r"
                        data-callback="JSF">
                    Register
                </button>
            </div>
        </form>
    </div>

@endsection