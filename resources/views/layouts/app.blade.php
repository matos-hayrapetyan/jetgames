<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="all" />
    <link rel="canonical" href="{{ nonSSL(Request::url()) }}" />
    @yield('meta')

    @yield('header-scripts')
    @include('layouts.tracking-scripts')
    @verbatim
        <script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "url": "https://jetgames.online/",
            "name": "jetgames.online",
            "alternateName": "jetgames",
            "potentialAction":
             {
                "@type": "SearchAction",
                "target": "http://jetgames.online/search/{search_term_string}",
                "query-input": "required name=search_term_string"
             }
        }
        </script>
    @endverbatim
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script async defer>
        window.L10n = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body class="@yield('body-classes')" >
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=148088025617343";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div id="app">
        @if($flash = session('message'))
            <div class="alert alert-success">
                {{ $flash }}
            </div>
        @endif

        @include('layouts.header')

        @yield('content')

        @include('layouts.errors')

    </div>

    <script src="{{ asset('js/jquery.min.js') }}" ></script>
    <script src="{{ asset('js/app.js') }}" ></script>
    <noscript id="deferred-styles">
        <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600|Roboto:300,400,500,700" rel="stylesheet" media="all"/>
    </noscript>
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>
    @yield('footer-scripts')
</body>
</html>