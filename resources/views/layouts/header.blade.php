<header id="header">
    @yield('before-header')
    <nav id="nav">
        <a class="brand" href="{{ url('/') }}">JetGames</a>
        <a class="nav-burger" href="#"><span></span></a>
        <ul id="header-main-menu" class="nav-items nav-left">

            <li><a {{ Request::is('games') ? 'class=active' : null }} href="{{ nonSSL(url('games')) }}">New Games</a></li>
            <li><a {{ Request::is('popular-games') ? 'class=active' : null }} href="{{ nonSSL(url('popular-games')) }}">Popular Games</a></li>
            <li class="hassubmenu">
                <span class="submenu-label">Tags&nbsp;<i class="material-icons">keyboard_arrow_down</i></span>
                <ul class="submenu">
                    @foreach($globalTags as $tag)
                        <li><a class="submenu-link" href="{{ nonSSL(url('/tags/'.$tag->slug)) }}">{{ $tag->name }}</a></li>
                    @endforeach
                    <li><a class="submenu-link" href="{{ nonSSL(url('/tags')) }}">View All</a></li>
                </ul>
            </li>
            <li><a {{ Request::is('wiki','wiki/*') ? 'class=active' : null }} href="{{ url('wiki') }}">Wiki</a></li>
        </ul>
        @if(Auth::check())
            <ul class="nav-items nav-right">
                @if(auth()->user()->hasRole('admin'))
                    <li ><a {{ Request::is('admin/games') ? 'class=active' : null }} href="{{ url('admin/games') }}" >Admin Panel</a></li>
                @endif
                <li class="nav-profile" ><a href="{{ url('control-panel') }}"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;&nbsp;{{ auth()->user()->name }}</a></li>
            </ul>
        @else
            <ul class="nav-items nav-right">
                <li class="nav-login"><a href="{{ url('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Login</a></li>
                <li class="nav-register"><a href="{{ url('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;&nbsp;Register</a></li>
            </ul>
        @endif
    </nav>
</header>