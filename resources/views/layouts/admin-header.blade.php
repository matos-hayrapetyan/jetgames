<header id="header">
    <nav id="nav"  class="admin">
        <a class="brand" href="{{ url('/') }}">JetGames</a>
        <ul class="nav-items nav-left">
            <li ><a {{ Request::is('admin/games') ? 'class=active' : null }} href="{{ url('admin/games') }}" >Games</a></li>
            <li ><a {{ Request::is('admin/tags') ? 'class=active' : null }} href="{{ url('admin/tags') }}" >Tags</a></li>
            @if(auth()->user()->hasRole('admin'))
                <li ><a {{ Request::is('wiki/create') ? 'class=active' : null }} href="{{ url('wiki/create') }}" target="_blank" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Publish an Article</a></li>
            @endif
        </ul>
        <ul class="nav-items nav-right">
            <li><a href="{{ url('control-panel') }}"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;&nbsp;{{ auth()->user()->name }}</a></li>
        </ul>
    </nav>
</header>