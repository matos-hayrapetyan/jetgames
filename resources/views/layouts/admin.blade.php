<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta name="robots" content="noindex">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="all" />
    @yield('meta')

    @yield('header-scripts')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script async defer>
        window.L10n = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>
<div id="app">
    @if($flash = session('message'))
        <div class="alert alert-success">
            {{ $flash }}
        </div>
    @endif

    @include('layouts.admin-header')

    @yield('content')

    @include('layouts.errors')

</div>

<script src="{{ asset('js/jquery.min.js') }}" ></script>
<script src="{{ asset('js/app.js') }}" ></script>
<noscript id="deferred-styles">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600|Roboto:400,500,700" rel="stylesheet" media="all"/>
</noscript>
<script>
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
</script>
@yield('footer-scripts')
</body>
</html>