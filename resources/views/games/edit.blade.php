@extends('layouts.admin')

@section('title', 'Edit a game')

@section('content')
    <div class="container margin-40-v card">
        <h1>Editing a game</h1>
        <h2 class="margin-20-v"><a href="{{ nonSSL(url('games/'.$game->slug)) }}">View game</a></h2>
        <form action="{{ url('games/'.$game->slug) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <input type="hidden" name="game_id" value="{{ $game->id }}" />
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="{{ $game->name }}" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug"  value="{{ $game->slug }}" />
            </div>
            <div class="form-field">
                <label for="status">Status</label>
                <select name="status" id="status">
                    <option value="published" @if($game->status === 'published') selected @endif>Published</option>
                    <option value="draft" @if($game->status === 'draft') selected @endif>Draft</option>
                </select>
            </div>
            <div class="form-field">
                <label for="requires_premium">Requires premium subscription&nbsp;<input type="checkbox" name="requires_premium" id="requires_premium" value="1" @if($game->requires_premium) checked @endif /></label>
            </div>
            <div class="form-field">
                <input type="file" id="mediaUpload" @change="uploadFile" />
                <div v-if="mediaSrc">@{{ mediaSrc }}</div>
                <label for="content">Content</label>
                <textarea name="content" id="content">{{ $game->content }}</textarea>
            </div>
            <div class="form-field">
                <label for="description">Description</label>
                <textarea name="description" id="description">{{ $game->description }}</textarea>
            </div>
            <div class="form-field">
                <label for="tag_id">Tag</label>
                <select id="edit_tags" name="tag_id[]" multiple="multiple" v-model="selectedTags" size="10" data-default="{{json_encode($tags)}}">
                    <option value="">&#8211;Select&#8211;</option>
                    <option v-for="tag in tags" v-bind:value="tag.id" >
                        @{{ tag.name }}
                    </option>
                </select>
            </div>
            <div class="form-field">
                <div class="form-field">
                    <label for="tag_id">Featured Tag</label>
                    <select id="edit_featured_tag" name="featured_tag" v-model="selectedFeaturedTag" data-default="{{ null!==$game->featured_tag?$game->featured_tag->id:'' }}">
                        <option value="">&#8211;Select&#8211;</option>
                        <option v-for="tag in tags" v-if="inArray(tag.id,selectedTags)" v-bind:value="tag.id">
                            @{{ tag.name }}
                        </option>
                    </select>

                </div>
            </div>
            <div>
                <a class="margin-20-v button button-white" href="{{ url('tags/create') }}" target="_blank"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create new tag</a>
            </div>
            <div class="form-field">
                <label for="image">Featured Image(180x135)</label>
                @if($game->image)
                    <div>
                        <img src="{{ $game->image->getFile() }}" />
                    </div>
                @endif
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <label for="image">Preview Image(180x135)</label>
                @if($game->preview_image)
                    <div>
                        <img src="{{ $game->preview_image->getFile() }}" />
                    </div>
                @endif
                <input type="file" name="preview_image" multiple="false" />
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection

@section('footer-scripts')
    <script src="{{ asset('js/create-game.js') }}"></script>
@endsection