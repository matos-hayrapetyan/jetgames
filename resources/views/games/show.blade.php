@extends('layouts.app')
@section('title', $game->name)

@section('meta')
    <meta name="description" content="{{ $game->name }} Play Free Online. Play Now {{ $game->name }} @if(!$game->tags->isEmpty()) {{ $game->tags[0]->name }} @endif game at JetGames" />
    <meta name="keywords" content="{{ $game->name }}, @if(!$game->tags->isEmpty()) {{ $game->tags[0]->name }} @endif, free games" />
@endsection

@section('content')

    <div class="single-game container margin-40-v">
        <div class="main-content card">
            <div class="text-center">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- my first ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-4664333478983479"
                     data-ad-slot="1232697682"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            @if(null !== $game->featured_tag)
                <h1 class="game-title margin-20-top">Play {{ $game->name }} - Play {{ $game->name }} Online, Free Online <a href="{{ nonSSL(url('tags/'.$game->featured_tag->slug)) }}">{{ $game->featured_tag->name }} Games</a></h1>
            @elseif(!$game->tags->isEmpty())
                <h1 class="game-title margin-20-top">Play {{ $game->name }} - Play {{ $game->name }} Online, Free Online <a href="{{ nonSSL(url('tags/'.$game->tags[0]->slug)) }}">{{ $game->tags[0]->name }} Games</a></h1>
            @else
                <h1 class="game-title margin-20-top">Play {{ $game->name }} - Play {{ $game->name }} Online, Free <a href="{{ nonSSL(url('tags/')) }}">Online Games</a></h1>
            @endif
            @if(Auth::check() && auth()->user()->hasRole('admin'))
                    <h2 class="margin-40-bottom"><a href="{{ nonSSL(url('games/'.$game->slug.'/edit')) }}">Edit Game</a></h2>
            @endif
            @if(request()->isSecure())
                <div style="background-color:#FDD835; padding: 10px 36px 10px 36px;">
                    If you see blank space instead of the game, please try <a style="color:#2196F3;" href="{{ nonSSL(request()->url()) }}">this</a> version of the page
                </div>
            @endif
            <div class="game-content">
                {!! $game->content !!}
            </div>
            <div class="margin-40-v game-description">
                <h2>{{ $game->name }} Description</h2>
                <div class="text-content">
                    {!! $game->description !!}
                </div>
            </div>
            <div class="game-comments">
                <div class="fb-comments" data-href="{{ nonSSL(Request::url()) }}" data-width="100%"></div>
            </div>
        </div>
        <div class="sidebar">

            <div class="card">
                <h2>Game Tags</h2>
                <div class="tag-list">
                    @foreach($game->tags as $tag)
                        @if(null !== $game->image)
                        <a class="tag" href="{{ nonSSL(url('/tags/'.$tag->slug)) }}"><img src="{{ $tag->image->getFile() }}" alt="{{$tag->name}}" />&nbsp;<b>{{ $tag->name }}</b></a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="card margin-20-top">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- my first ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-4664333478983479"
                     data-ad-slot="1232697682"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            @if(count($similar_games))
            <div class="card margin-20-top game-list">
                <h2>Similar Games</h2>
                @foreach($similar_games as $similar_game)
                    <p><a class="game" href="{{ nonSSL(url('/games/'.$similar_game->slug)) }}"><img width="60" src="{{ $similar_game->image->getFile() }}" alt="{{$similar_game->name}}" />&nbsp;&nbsp;<b>{{ $similar_game->name }}</b></a></p>
                @endforeach
            </div>
                @endif
        </div>
    </div>

@endsection