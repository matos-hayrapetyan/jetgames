@foreach($games as $game)
    <div class="card--game card card-small no-padding margin-10">
        <a href="{{ nonSSL(url('/games/'.$game->slug)) }}">
            @if(null !== $game->image)
            <div class="card-img" data-preview="{{ $game->preview_image->getFile() }}" data-img="{{ $game->image->getFile() }}">
                <img src="{{ $game->image->getFile() }}" alt="{{ $game->name }}"/>
            </div>
            @endif
            <div class="card-meta">
                <h2 class="card-head">{{ $game->name }}</h2>
            </div>
        </a>
    </div>
@endforeach
