@extends('layouts.app')

@section('title', 'Play Online Games, Free Online Games, Free Kids Online Games, Play Action Games, Arcade, PC Games')

@section('meta')
    <meta name="description"
          content="The most creative and exciting online games for all renaissance and sexes on a unique gaming portal jetgames.online. Here are some fascinating and exclusive novelties of the game world"/>
    <meta name="keywords" content="games, kids games, online games, play online games"/>
@endsection

@section('content')

    <div class="margin-40-top card container text-center">
        <h1>Play Online Games, Free Online Games, Free <a href="{{ url('/tags/kids') }}">Kids</a> Online Games, Play <a
                    href="{{ url('tags/action') }}">Action</a> Games, <a href="{{ url('tags/arcade') }}">Arcade</a>, PC
            Games</h1>
    </div>

    <div class="games-index-list container flex flex-grid margin-40-v" data-paged="{{ $games->currentPage() }}"
         data-total="{{ $games->lastPage() }}">
        @include('games.index')
    </div>
@endsection