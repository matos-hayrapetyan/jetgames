@extends('layouts.admin')

@section('title', 'Publish a new game')

@section('content')
    <div class="container margin-40-v card">
        <form action="{{ url('games') }}" method="post" enctype="multipart/form-data">
            <h1>Publish a new game</h1>
            {{ csrf_field() }}
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" />
            </div>
            <div class="form-field">
                <label for="status">Status</label>
                <select name="status" id="status">
                    <option value="published">Published</option>
                    <option value="draft">Draft</option>
                </select>
            </div>
            <div class="form-field">
                <label for="requires_premium">Requires premium subscription&nbsp;<input type="checkbox" name="requires_premium" id="requires_premium" value="1" /></label>
            </div>
            <div class="form-field">
                <input type="file" id="mediaUpload" @change="uploadFile" />
                <div v-if="mediaSrc">@{{ mediaSrc }}</div>
                <label for="content">Content</label>
                <textarea name="content" id="content"></textarea>
            </div>
            <div class="form-field">
                <label for="description">Description</label>
                <textarea name="description" id="description"></textarea>
            </div>
            <div class="form-field">
                <label for="tag_id">Tag</label>
                <select name="tag_id[]" multiple="multiple" size="10" v-model="selectedTags">
                    <option value="">&#8211;Select&#8211;</option>
                    <option v-for="tag in tags" v-bind:value="tag.id">
                        @{{ tag.name }}
                    </option>
                </select>
            </div>
            <div class="form-field">
                <label for="tag_id">Featured Tag</label>
                <select name="featured_tag" v-model="selectedFeaturedTag">
                    <option value="">&#8211;Select&#8211;</option>
                    <option v-for="tag in tags" v-if="inArray(tag.id,selectedTags)" v-bind:value="tag.id">
                        @{{ tag.name }}
                    </option>
                </select>

            </div>
            <div>
                <a class="margin-20-v button button-white" href="{{ url('tags/create') }}" target="_blank"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create new tag</a>
            </div>
            <div class="form-field">
                <label for="image">Featured Image(180x135)</label>
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <label for="image">Preview Image(180x135)</label>
                <input type="file" name="preview_image" multiple="false" />
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection

@section('footer-scripts')
    <script src="{{ asset('js/create-game.js') }}"></script>
@endsection