@extends('layouts.admin')

@section('title', 'Admin | Games')

@section('content')

    <div class="admin-wrap container margin-40-v">
        <div class="jetgames">
            <h1 class="-primary">All Games <a class="button button--secondary" href="{{ url('/games/create') }}">Add New</a></h1>
        </div>
        <ul class="mat-list">
            @foreach($games as $game)
                <li class="mat-list-item">
                    <a class="mat-list-item-link" href="{{ url('/games/'.$game->slug.'/edit') }}">
                        @if(null !== $game->image)
                            <span class="mat-list-item-start"><img src="{{ $game->image->getFile() }}"
                                                                   alt="{{ $game->name }}"/></span>
                        @endif
                        <span class="mat-list-item-text">
                            <span class="mat-list-item-title">{{ $game->name }}
                                @if('published' === $game->status)
                                    <span class="status published">(Published)</span>
                                @elseif('draft' === $game->status)
                                    <span class="status draft">(Draft)</span>
                                @endif
                            </span>
                            <span class="mat-list-item-text-secondary">
                                {{
                                    implode('/ ', $game->tags->map(function($t){
                                        return $t->name;
                                    })->toArray())
                                }}
                            </span>
                        </span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

@endsection