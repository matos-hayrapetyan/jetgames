@extends('layouts.admin')

@section('title', 'Admin | Games')

@section('content')

    <div class="admin-wrap container margin-40-v">
        <div class="jetgames">
            <h1 class="-primary">All Tags <a class="button button--secondary" href="{{ url('/tags/create') }}">Add New</a></h1>
        </div>

        <ul class="mat-list">
            @foreach($tags as $tag)
                <li class="mat-list-item">
                    <a class="mat-list-item-link" href="{{ url('/tags/'.$tag->slug.'/edit') }}">
                        <span class="mat-list-item-start"><img src="{{ $tag->image->getFile() }}" alt="{{ $tag->name }}"/></span>
                        <span class="mat-list-item-text">
                            <span class="mat-list-item-title">{{ $tag->name }}</span>
                            <span class="mat-list-item-text-secondary">{{ $tag->games_count.' Games' }}</span>
                        </span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

@endsection