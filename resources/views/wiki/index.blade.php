@extends('layouts.app')


@section('title', 'Online Games Encyclopedia in jetgames.online')

@section('meta')
    <meta name="description" content="Online games industry has a lot of interesting information that we collect and share with you"/>
    <meta name="keywords" content="online games, online games information, online games encyclopedia, play online games" />
@endsection

@section('content')

    <div class="margin-40-top card container text-center">
        <h1>Online Games Encyclopedia</h1>
    </div>

    <div class="articles-index-list container flex flex-grid margin-40-v" data-paged="{{ $articles->currentPage() }}" data-total="{{ $articles->lastPage() }}">
        @include('wiki.articles.index')
    </div>

@endsection