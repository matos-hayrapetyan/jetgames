@extends('layouts.app')

@section('title', $article->name)


@section('meta')
    <meta name="description" content="{{ $article->meta_description }}" />
    <meta name="keywords" content="{{ $article->meta_keywords }}" />
@endsection

@section('content')
    <div class="container margin-40-v card">
        <div class="text-content">
            <div class="text-center">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- my first ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-4664333478983479"
                     data-ad-slot="1232697682"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <h1 class="margin-20-v">{{ $article->name }}</h1>
            @if(Auth::check() && auth()->user()->hasRole('admin'))
                <h2 class="margin-40-bottom"><a href="{{ url('wiki/'.$article->slug.'/edit') }}">Edit Article</a></h2>
            @endif
            {!! $article->content !!}
        </div>
        <div class="article-comments">
            <div class="fb-comments" data-href="{{ Request::url() }}" data-width="100%"></div>
        </div>
    </div>

@endsection