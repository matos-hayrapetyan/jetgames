@extends('layouts.admin')

@section('title', 'Publish a new article')

@section('content')
    <div class="container margin-40-v card">
        <form action="{{ url('wiki') }}" method="post" enctype="multipart/form-data">
            <h1>Publish a new article</h1>
            {{ csrf_field() }}
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" />
            </div>
            <div class="form-field">
                <label for="meta_description">Meta Description</label>
                <input type="text" name="meta_description" id="meta_description" />
            </div>
            <div class="form-field">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" id="meta_keywords" />
            </div>
            <div class="form-field">
                <input type="file" id="mediaUpload" @change="uploadFile" />
                <div v-if="mediaSrc">@{{ mediaSrc }}</div>
                <label for="content">Content</label>
                <textarea name="content" id="content"></textarea>
            </div>
            <div class="form-field">
                <label for="image">Featured Image(980x400)</label>
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection

@section('footer-scripts')
    <script src="{{ asset('js/create-game.js') }}"></script>
@endsection