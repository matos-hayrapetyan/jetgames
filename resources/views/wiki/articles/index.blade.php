@foreach($articles as $article)
    <div class="card--game card card-medium no-padding margin-10 text-center">
        <a href="/wiki/{{ $article->slug }}">
            <div class="card-img">
                <img src="{{ $article->image->getFile() }}" alt="{{ $article->name }}"/>
            </div>
            <div class="card-meta">
                <h2 class="card-head">{{ $article->name }}</h2>
            </div>
        </a>
    </div>
@endforeach