@extends('layouts.admin')

@section('title', 'Editing '.$article->name)

@section('content')
    <div class="container margin-40-v card">
        <h1>Editing {{ $article->name }}</h1>
        <h2 class="margin-20-v"><a href="{{ url('wiki/'.$article->slug) }}">View article</a></h2>
        <form action="{{ url('wiki/'.$article->slug) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <input type="hidden" name="article_id" value="{{ $article->id }}" />
            <div class="form-field">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="{{ $article->name }}" />
            </div>
            <div class="form-field">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" value="{{ $article->slug }}" />
            </div>
            <div class="form-field">
                <label for="meta_description">Meta Description</label>
                <input type="text" name="meta_description" id="meta_description" value="{{ $article->meta_description }}" />
            </div>
            <div class="form-field">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" id="meta_keywords" value="{{ $article->meta_keywords  }}" />
            </div>
            <div class="form-field">
                <input type="file" id="mediaUpload" @change="uploadFile" />
                <div v-if="mediaSrc">@{{ mediaSrc }}</div>
                <label for="content">Content</label>
                <textarea name="content" id="content">{{ $article->content }}</textarea>
            </div>
            <div class="form-field">
                <label for="image">Featured Image(980x400)</label>
                @if($article->image)
                    <div>
                        <img src="{{ $article->image->getFile() }}" />
                    </div>
                @endif
                <input type="file" name="image" />
            </div>
            <div class="form-field">
                <input class="button" type="submit" value="Save" />
            </div>
        </form>
    </div>

@endsection

@section('footer-scripts')
    <script src="{{ asset('js/create-game.js') }}"></script>
@endsection