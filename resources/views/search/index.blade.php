@extends('layouts.app')

@section('title','Search for '.$keyword.',Play Online Games, Free Online Games')
@section('meta')
    <meta name="description" content="The most creative and exciting online games for all renaissance and sexes on a unique gaming portal jetgames.online. Here are some fascinating and exclusive novelties of the game world"/>
    <meta name="keywords" content="games, kids games, online games, play online games" />
@endsection

@section('content')
    <div class="games-index-list container flex flex-grid margin-40-v" data-paged="{{ $games->currentPage() }}" data-total="{{ $games->lastPage() }}">
        @include('games.index')
    </div>
@endsection