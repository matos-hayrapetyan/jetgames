@extends('layouts.app')

@section('header-scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('title', 'Control Panel')

@section('content')
    <div class="margin-40-v card container">
        <div class="text-content">
            <h1>Hi, {{ auth()->user()->name }}</h1>
            <div class="cpanel-premium-status">
                @if(auth()->user()->isPremium())
                    <span class="cpanel=premium-active"></span>
                @else
                    <a class="button button--premium" href="#">Activate Premium</a>
                @endif

            </div>
            <ul>
                <li>email: {{ auth()->user()->email }}</li>
                <li>username: {{ auth()->user()->name }}</li>
                <li>Registered at: {{ auth()->user()->created_at }}</li>
                <li>Status : {{ implode(', ',
                auth()
                ->user()
                ->roles()
                ->get()
                ->map(function ($role) {
                    return $role->name;
                    })
                    ->toArray() ) }}</li>
            </ul>

            <div>
                <a href="{{ url('/logout') }}" class="button">Logout</a>
            </div>
        </div>
    </div>
@endsection