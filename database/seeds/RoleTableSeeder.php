<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'A Admin User';
        $role_admin->save();

        $role_member = new Role();
        $role_member->name= 'member';
        $role_member->description= 'A Member';
        $role_member->save();

        $users = \App\User::all();

        foreach ($users as $user) {
            $user->roles()->attach($role_admin);
        }

    }
}
