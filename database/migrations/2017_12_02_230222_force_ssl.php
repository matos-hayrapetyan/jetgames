<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForceSsl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $games = \App\Game::all();
        $tags = \App\Tag::all();
        $articles = \App\Article::all();

        foreach($games as $game){
            $game->content = preg_replace("/http:\/\/jetgames/i", "https://jetgames", $game->content);
            $game->save();
        }

        foreach ($tags as $tag){
            $tag->description = preg_replace("/http:\/\/jetgames/i", "https://jetgames", $tag->description);
            $tag->save();
        }

        foreach ($articles as $article){
            $article->content = preg_replace("/http:\/\/jetgames/i", "https://jetgames", $article->content);
            $article->save();
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
